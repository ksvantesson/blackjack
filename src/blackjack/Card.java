package blackjack;

public class Card {

    public enum Suit {
        SPADES, HEARTS, DIAMONDS, CLUBS
    };
    
    public static final int ACE = 1;
    public static final int JACK = 11;
    public static final int QUEEN = 12;
    public static final int KING = 13;
    
    private final Suit suit;
    private final int rank;
    
    public Card(Suit suit, int rank) {
        this.suit = suit;
        this.rank = rank;
    }
    
    public Suit getSuit() {
        return suit;
    }
    
    public int getRank() {
        return rank;
    }
    
    @Override
    public String toString() {
        return suitAsString() + rankAsString();
    }
    
    private String suitAsString() {
        switch (suit) {
            case SPADES:
                return "S";
                
            case HEARTS:
                return "H";
                
            case DIAMONDS:
                return "D";
                
            case CLUBS:
                return "C";
        }
        
        return null;
    }

    private String rankAsString() {
        switch (rank) {
            case ACE:
                return "A";

            case JACK:
                return "J";

            case QUEEN:
                return "Q";

            case KING:
                return "K";

            default:
                return String.valueOf(rank);
        }
    }
}
