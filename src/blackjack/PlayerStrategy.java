package blackjack;

public class PlayerStrategy implements HandStrategy {
    
    private BlackJackEvents events;

    public PlayerStrategy(BlackJackEvents events) {

        this.events = events;
    }
    
    @Override
    public boolean takeNextTurn(Hand hand) {
        return events.takeAnotherCard(hand);
    }
}