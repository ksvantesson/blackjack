package blackjack;

import java.util.ArrayList;

public class Deck {
    
    private ArrayList<Card> cards = new ArrayList<>();
    
    public Deck() {
        
    }
    
    public Card dealCard() {
        if (cards.isEmpty()) {
            InitializeCards();
        }
            
        Card card = cards.get((int) (Math.random()*cards.size()));
        
        cards.remove(card);
        
        return card;
        
    }

    private void InitializeCards() {
        System.out.println("New cards");
        
        for (int i = 1; i <= 13; i++) {
            cards.add(new Card(Card.Suit.SPADES, i));
            cards.add(new Card(Card.Suit.HEARTS, i));
            cards.add(new Card(Card.Suit.DIAMONDS, i));
            cards.add(new Card(Card.Suit.CLUBS, i));
        }
    }
}
