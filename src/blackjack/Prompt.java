package blackjack;

import java.util.Scanner;

public class Prompt {
    
    private Scanner scanner;
    
    public Prompt() {
        scanner = new Scanner(System.in);
    }
    
    public boolean ask(String question, String answer) {
        System.out.print(question);

        String input = scanner.next();
        
        return input.equals(answer);
    }
    
    public void close() {
        scanner.close();
    }
}
