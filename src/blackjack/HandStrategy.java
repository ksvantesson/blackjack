package blackjack;

public interface HandStrategy {
    
    boolean takeNextTurn(Hand hand);
}
