/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack;


public interface BlackJackEvents {

    void cardDealt(Hand hand, Card card);

    void goingBust(Hand hand);

    void isLoser(Hand hand);

    void isWinner(Hand hand);

    boolean shouldQuitGame();

    void startGame();

    boolean takeAnotherCard(Hand hand);
    
    void endGame();    
}
