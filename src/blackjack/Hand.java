package blackjack;

import java.util.ArrayList;

public class Hand {
    
    private ArrayList<Card> cards = new ArrayList<>();
    private String name;
    private BlackJackEvents events;
    private HandStrategy strategy;
    
    public Hand(String name, BlackJackEvents events, HandStrategy strategy) {
        this.name = name;
        this.events = events;
        this.strategy = strategy;
    }
    
    public void initialDeal(Deck deck) {
        cards.clear();
        turn(deck);
    }
    
    public void doTurns(Deck deck) {
        while (takeNextTurn()) {
            turn(deck);
        }
    }

    private boolean takeNextTurn() {
        
        return cards.size() < 2 || !isBust() && strategy.takeNextTurn(this);
    }
    
    private void turn(Deck deck) {
        dealCard(deck);
        
        if (isBust()) {
            events.goingBust(this);
        }
    }
    
    private void dealCard(Deck deck) {
        Card card = deck.dealCard();
        
        cards.add(card);
        events.cardDealt(this, card);
    }
    
    public int getScore() {

        int score = 0;
        boolean anyAces = false;

        for (Card card : cards) {
            if (card.getRank() >= 10)
                score += 10;
            else if (card.getRank() == Card.ACE) {
                score += 1;
                anyAces = true;
            }
            else
                score += card.getRank();
        }
        
        if (anyAces && score <= 11)
            score += 10;
        
        return score;
    }
    
    public boolean isBust() {
        return getScore() > 21;
    }

    public String getName() {
        return name;
    }
}
