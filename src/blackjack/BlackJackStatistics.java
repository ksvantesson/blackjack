package blackjack;

public class BlackJackStatistics extends BlackJackEventsDecorator {

    private int playerWins;
    private int playerLosts;
    private Logger log;
    
    public BlackJackStatistics(BlackJackEvents component, Logger log) {
        super(component);
        
        this.log = log;
    }

    @Override
    public void isLoser(Hand hand) {
        super.isLoser(hand);
        
        playerLosts++;
    }

    @Override
    public void isWinner(Hand hand) {
        super.isWinner(hand);
        
        playerWins++;
    }

    @Override
    public void endGame() {
        super.endGame();
        
        log.info(String.format("Players won %d times and lost %d times.", playerWins, playerLosts));
        
        if (playerWins >= playerLosts)
            log.warning("Players are winning more than they are losing.");
    }
}
