package blackjack;

public class BlackJackCommandline implements BlackJackEvents {
    
    Prompt prompt;
    
    public BlackJackCommandline() {
        this.prompt = new Prompt();
    }
    
    @Override
    public void startGame() {
        System.out.println("Welcome to Black Jack");
    }
    
    @Override
    public void cardDealt(Hand hand, Card card) {
        System.out.println(String.format("%s got %s, score is now %d.", hand.getName(), card,hand.getScore()));
    }
    
    @Override
    public void goingBust(Hand hand) {
        System.out.println(hand.getName() + " going bust");
    }
    
    @Override
    public boolean takeAnotherCard(Hand hand) {
        return prompt.ask("@" + hand.getName() + ": Do you want to (h)it or (s)tand? ", "h");
    }
    
    @Override
    public void isWinner(Hand hand) {
        System.out.println(hand.getName() + " wins!");
    }
    
    @Override
    public void isLoser(Hand hand) {
        System.out.println(hand.getName() + " loses!");
    }
    
    @Override
    public boolean shouldQuitGame() {
        return prompt.ask("Do you want to play again (y/n)? ", "n");
    }

    @Override
    public void endGame() {
        System.out.println("Thank you for playing.");
        
        prompt.close();
    }
}
