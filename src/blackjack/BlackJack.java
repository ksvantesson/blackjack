/*
   Här är min version av Black Jack. Koden innehåller åtminstone tre fel, och tre-fyra
   tveksamheter. Dessutom finns det ett par saker som skulle kunna förbättras.

   Studera koden, försök att hitta alla fel och konstigheter. Fundera också på vilka 
   förbättringar som skulle kunna göras. Om det är något i koden som du inte förstår, 
   så var inte orolig. Jag kommer gå igenom alla detaljer.

   Koden kommer vara utgångspunkt när vi börjar med objektorienterad programmering
   och tillsammans kommer vi dela upp den i lämpliga klasser.
*/

package blackjack;

import java.util.ArrayList;

public class BlackJack implements HandStrategy {
    
    BlackJackEvents events;
    ArrayList<Hand> players = new ArrayList<>();
    Hand dealerHand;
    Deck deck = new Deck();
    
    public BlackJack(Logger log) {
        events = new BlackJackStatistics(new BlackJackLogger(new BlackJackCommandline(), log), log);
        dealerHand = new Hand("Dealer", events, this);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Logger log = new Logger();
        
        log.setLevel(Logger.INFO);
        
        try {
            BlackJack game = new BlackJack(log);

            game.run();
        }
        catch (Exception ex) {
            log.error(ex.toString());
        }
    }
    
    private void run() {

        players.add(new Hand("Player 1", events, new PlayerStrategy(events)));
        players.add(new Hand("Player 2", events, new HandStrategy() {
            @Override
            public boolean takeNextTurn(Hand hand) {
                return hand.getScore() <= 18;
            }
        }));
        
        events.startGame();
        
        while (true) {
            
            initialDeal();
            doTurns();
            printWinner();
            
            if (events.shouldQuitGame())
                break;
        }
        
        events.endGame();
    }

    private void initialDeal() {
        dealerHand.initialDeal(deck);
        
        for (Hand player : players) {
            player.initialDeal(deck);
        }
    }

    private void doTurns() {
        for (Hand player : players) {
            player.doTurns(deck);
        }
        
        dealerHand.doTurns(deck);
    }

    private void printWinner() {
        for (Hand player : players)
        {
            if (winsAgainst(player))
                events.isLoser(player);
            else
                events.isWinner(player);
        }
    }
    
    private boolean winsAgainst(Hand player) {
        return player.isBust() || !dealerHand.isBust() && dealerHand.getScore() >= player.getScore();
    }
    
    @Override
    public boolean takeNextTurn(Hand hand) {
        return anyPlayersLeft() && dealerHand.getScore() <= 16;
    }
    
    private boolean anyPlayersLeft() {
        
        for (Hand player : players) {
            if (!player.isBust())
                return true;
        }
        
        return false;
    }
}
