package blackjack;

public class BlackJackEventsDecorator implements BlackJackEvents {
    
    private BlackJackEvents component;
    
    public BlackJackEventsDecorator(BlackJackEvents component) {
        this.component = component;
    }

    @Override
    public void cardDealt(Hand hand, Card card) {
        component.cardDealt(hand, card);
    }

    @Override
    public void goingBust(Hand hand) {
        component.goingBust(hand);
    }

    @Override
    public void isLoser(Hand hand) {
        component.isLoser(hand);
    }

    @Override
    public void isWinner(Hand hand) {
        component.isWinner(hand);
    }

    @Override
    public boolean shouldQuitGame() {
        return component.shouldQuitGame();
    }

    @Override
    public void startGame() {
        component.startGame();
    }

    @Override
    public boolean takeAnotherCard(Hand hand) {
        return component.takeAnotherCard(hand);
    }

    @Override
    public void endGame() {
        component.endGame();
    }
}
