package blackjack;

public class BlackJackLogger extends BlackJackEventsDecorator {

    private Logger log;
    
    public BlackJackLogger(BlackJackEvents component, Logger log) {
        super(component);
        this.log = log;
    }

    @Override
    public void cardDealt(Hand hand, Card card) {
        super.cardDealt(hand, card);
        
        log.info(hand.getName() + " got " + card);
    }

    @Override
    public void goingBust(Hand hand) {
        super.goingBust(hand);
        
        log.info(hand.getName() + " going bust");
    }

    @Override
    public void isLoser(Hand hand) {
        super.isLoser(hand);
        
        log.info(hand.getName() + " lost");
    }

    @Override
    public void isWinner(Hand hand) {
        super.isWinner(hand);
        
        log.info(hand.getName() + " won");
    }

    @Override
    public boolean takeAnotherCard(Hand hand) {
        boolean anotherCard = super.takeAnotherCard(hand);
        
        if (anotherCard)
            log.info(hand.getName() + " chooses to hit");
        else
            log.info(hand.getName() + " chooses to stand");
        
        return anotherCard;
    }
    
    
    
    

}
