package blackjack;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {
    
    public static final int NONE = 0;
    public static final int ERROR = 1;
    public static final int WARNING = 2;
    public static final int INFO = 3;
    
    private int logLevel = WARNING;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public void error(String logEntry) {
        log(ERROR, logEntry);
    }
    
    public void warning(String logEntry) {
        log(WARNING, logEntry);
    }
    
    public void info(String logEntry) {
        log(INFO, logEntry);
    }
    
    public void setLevel(int logLevel) {
        this.logLevel = logLevel;
    }
    
    private void log(int level, String entry) {
        
        if (level > logLevel)
            return;
        
        BufferedWriter writer = null;
        
        try {
            writer = new BufferedWriter(new FileWriter("log.txt", true));
            writer.write(String.format("%s %s: %s", getLogDate(), getLogLevelAsString(level), entry));
            writer.newLine();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            try {
                // Close the writer regardless of what happens...
                writer.close();
            }
            catch (Exception e) {
            }
        }
    }

    private String getLogDate() {
        return dateFormat.format(new Date());
    }
    
    private String getLogLevelAsString(int level) {
        switch (level) {
            case ERROR:
                return "ERROR";
                
            case WARNING:
                return "WARNING";
                
            case INFO:
                return "INFO";
                
            default:
                return "UNKNOWN LEVEL";
        }
    }
}
